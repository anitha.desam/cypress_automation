const csv = require('csvtojson')
const fs = require('fs');

const sourceDir = './cypress/fixtures/csv_files/Consumer/';
const destDir = './cypress/fixtures/json_files/Consumer/';
const dir = fs.opendirSync(sourceDir)

if(fs.existsSync(destDir)) {
    fs.rmdirSync(destDir, { recursive: true });
}

fs.mkdirSync(destDir, { recursive: true });

let dirent;

while ((dirent = dir.readSync()) !== null) {
    let fileName = dirent.name
    csv().fromFile(sourceDir + fileName).then((jsonObj) => {
        let outputJson = {};
        outputJson.testScenarios = jsonObj;
        fs.writeFileSync(destDir + (fileName.replace('.csv', '.json')), JSON.stringify(outputJson))

    })
}
dir.closeSync()
