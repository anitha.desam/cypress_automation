///<reference types = "Cypress"/>
let testData = require('../../../../fixtures/json_files/Consumer/plentiTierRules.json')
let apiConfigData = require('../../../../fixtures/turboQuoteapi.json')
let hook = require('../../../../support/hook.js')

describe('Plenti Tier rules testCases', () => {
    const plentiTierTests = testData.testScenarios;

    plentiTierTests.forEach((testCase) => {

        let amountRequired = 20000

        it(testCase.testCaseName, () => {
            cy.request({
                url: apiConfigData.turboQuoteApi,
                method: apiConfigData.method,
                headers: {
                    'x-api-key': apiConfigData.xApiKey,
                    'content-type': apiConfigData.contentType
                },
                body:
                {
                    "purchasePrice": amountRequired,
                    "condition": "New",
                    "year": 2020,
                    "term": 60,
                    "assetType": "Car",
                    "financeType": "ConsumerLoan",
                    "depositAmount": (testCase.depositAmountPercent == "")?null:(testCase.depositAmountPercent*amountRequired/100),
                    "residentialStatus": testCase.livingSituation,
                    "propertyOwner": (testCase.livingSituation == "Homeowner") ? true : false,
                    "creditScoreComprehensive": testCase.creditScoreComp,
                    "employmentStatus": testCase.employmentStatus,
                    "totalMonthsOfEmployment": testCase.currentEmploymentDurationInMonths
                }

            }).then((response) => {
                var lenderQuotes = response.body.quotes

                var ratesetterLender = null;

                for (let x = 0; x < lenderQuotes.length; x++) {

                    if (lenderQuotes[x].lenderCode == "ratesetter") {
                        ratesetterLender = lenderQuotes[x];
                    
                        if (ratesetterLender) {
                            if (ratesetterLender.canQuote == true) {
                                expect(ratesetterLender.lenderProductTier).to.equal(testCase.expectedTier);
        
                            }else{
                                expect(ratesetterLender.canQuote).to.equal(false)
                            }
        
        
                        } else {
                            throw new Error("No result for ratesetter lender for the provided request")
                        }
                    }
        

                   

                }

               

            })

        })

    })

})