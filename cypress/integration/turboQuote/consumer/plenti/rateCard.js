///<reference types = "Cypress"/>
let testData = require('../../../../fixtures/json_files/Consumer/rateSetterRateCard.json')
let apiConfigData = require('../../../../fixtures/turboQuoteapi.json')
let hook = require('../../../../support/hook.js')

describe('RateSetter RateCard Test', () => {
    const rateCardTests = testData.testScenarios;

    rateCardTests.forEach((testCase) => {
        
        let amountRequired = 50000

        it(testCase.testCaseName, () => {
            cy.request({
                url: apiConfigData.turboQuoteApi,
                method: apiConfigData.method,
                headers: {
                    'x-api-key': apiConfigData.xApiKey,
                    'content-type': apiConfigData.contentType
                },
                body:
                {   
                    "year": (new Date().getFullYear() - testCase.assetAge),
                    "propertyOwner": testCase.propertyOwner,
                    "creditScoreComprehensive": testCase.creditScoreComprehensive,
                    "employmentStatus": testCase.employmentStatus,
                    "totalMonthsOfEmployment": testCase.currentEmploymentDurationInMonths,
                    "requestedLoanAmount": amountRequired,
                    "condition": testCase.vehicleCondition,
                    "term": testCase.term,
                    "assetType": "Car",
                    "financeType": "ConsumerLoan",

                }

            }).then((response) => {
                var quoteSize = response.body.quotes
               
                for (let x = 0; x < quoteSize.length; x++) {
                    if(quoteSize[x].lenderCode == "ratesetter" && quoteSize[x].lenderProductTier == testCase.tier){
                    
                        console.log(quoteSize[x].lenderProductTier)
                    expect(quoteSize[x].floorRate).to.eq(parseFloat(testCase.interestRateMin))
                    
                    expect(quoteSize[x].ceilingRate).to.eq(parseFloat(testCase.interestRateMax))
                    //expect(quoteSize[x].lenderProductCode).to.eq(testCase.lenderProductCode)
                   // expect(quoteSize[x].brokerageIncGst).to.be.closeTo(hook.macCommercialMaxBrokerage(testCase.maxBrokerage,amountRequired+1340),0.4)
                    console.log(hook.CalculateComparisonRate(1340, 8.25, quoteSize[x].floorRate, 60))
                    console.log(quoteSize[x].comparisonRate)
                           
                        
                    }

                }
            

            })
 
        })

    })

})