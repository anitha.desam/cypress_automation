///<reference types = "Cypress"/>
let testData = require('../../../../../fixtures/json_files/Consumer/pepperMarineRateCard.json')
let defaultTestData = require('../../../../../fixtures/defaultTestData.json')

describe('Pepper Marine Rate-Card', () => {

    const pepperMarineRateCardTests = testData.testScenarios;
    beforeEach(() => {
        cy.visit("/")
    })
    pepperMarineRateCardTests.forEach((testCase) => {

        it(testCase.lenderName + ", Asset Year - " + testCase.manufactureYear + ", " + testCase.testCaseName, () => {

            cy.assetTypeSelect("Marine")
            cy.financeTypeSelect(defaultTestData.financeType)
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(testCase.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(testCase.manufactureYear)

            // --------Customer--------

           if(testCase.employmentStatus == ''){
            cy.employmentStatusSelect(defaultTestData.employmentStatus)
           }else{
            cy.employmentStatusSelect(testCase.employmentStatus)
           }
            
            cy.livingSituationSelect(testCase.propertyOwner)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- Finance details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
            //cy.provideLvr(defaultTestData.lvr)
            cy.provideTerm(defaultTestData.termInYears)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert
           
            cy.checkTheInputFieldInterestValue(testCase.lenderName,parseFloat(testCase.interestRateMax))
            cy.checkTheInterestValue(testCase.lenderName, testCase.interestRateMin, testCase.interestRateMax)

        })

        it(testCase.lenderName + ", Asset Year - " + testCase.manufactureYear + ", LoanTerm = 6years " + testCase.testCaseName, () => {

            cy.assetTypeSelect("Marine")
            cy.financeTypeSelect(defaultTestData.financeType)
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(testCase.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(testCase.manufactureYear)

            // --------Customer--------

           if(testCase.employmentStatus == ''){
            cy.employmentStatusSelect(defaultTestData.employmentStatus)
           }else{
            cy.employmentStatusSelect(testCase.employmentStatus)
           }
            
            cy.livingSituationSelect(testCase.propertyOwner)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- Finance details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
           // cy.provideLvr(defaultTestData.lvr)
            cy.provideTerm(defaultTestData.termInYears+1)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert
            
            cy.checkTheInputFieldInterestValue(testCase.lenderName,parseFloat(testCase.interestRateMax)+0.5)
            cy.checkTheInterestValue(testCase.lenderName, (parseFloat(testCase.interestRateMin)+0.5).toString(), (parseFloat(testCase.interestRateMax)+0.5).toString())

        })
    })
})