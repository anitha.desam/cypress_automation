///<reference types = "Cypress"/>
let testData = require('../../../../../fixtures/json_files/Consumer/pepperTierRules.json')
let defaultTestData = require('../../../../../fixtures/defaultTestData.json')

describe('Pepper Consumer Tier Validation', () => {

    const pepperConsumerTierTests = testData.testScenarios;
    beforeEach(() => {
        cy.visit("/")
    })
    pepperConsumerTierTests.forEach((testCase) => {

        it(testCase.lenderAndTier + "\n Condition: " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect(defaultTestData.financeType)
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(defaultTestData.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(defaultTestData.manufactureYear)

            // --------Customer--------

            cy.employmentStatusSelect(defaultTestData.employmentStatus)
            cy.livingSituationSelect(testCase.livingSituation)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)

            if (testCase.savings == "") {

            } else {
                cy.addSavings(testCase.savings)
            }

            if (testCase.hadPreviousFinanceSelect == "") {

                cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            } else {

                cy.get("div[role=button]").eq(22).click();
                cy.contains("Yes").next().click();
            }

            //(testCase.hadPreviousFinanceSelect == "") ? cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance) : cy.hadPreviousFinanceSelect(testCase.hadPreviousFinance)

            cy.creditHistorySelect(testCase.creditHistory)

            //----- Finance details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
            //cy.provideLvr(defaultTestData.lvr)
            cy.provideTerm(defaultTestData.termInYears)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert

            cy.validateLenderAndLenderTier(testCase.lenderName, testCase.lenderAndTier)

            if (testCase.lenderName == "PepperPeB1" || "PepperPeC1") {

                cy.get("[name = PepperPeA1]").find("span").then(($lender) => {
                    if ($lender.text().includes("Cannot Quote !")) {

                        expect($lender.text()).to.not.include("Rate chart used")
                    }

                })
            }

            if (testCase.lenderName == "PepperPeC1") {

                cy.get("[name = PepperPeB1]").find("span").then(($lender) => {
                    if ($lender.text().includes("Cannot Quote !")) {

                        expect($lender.text()).to.not.include("Rate chart used")
                    }

                })
            }
        })

    })
})