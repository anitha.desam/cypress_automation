///<reference types = "Cypress"/>
let testData = require('../../../../../fixtures/json_files/Consumer/pepperRateCard.json')
let defaultTestData = require('../../../../../fixtures/defaultTestData.json')

describe('Pepper Consumer Rate-Card', () => {

    const pepperRateCardTests = testData.testScenarios;
    beforeEach(() => {
        cy.visit("/")
    })
    pepperRateCardTests.forEach((testCase) => {

        it(testCase.lenderName + ", Asset Year - " + testCase.manufactureYear + ", " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect(defaultTestData.financeType)
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(testCase.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(testCase.manufactureYear)

            // --------Customer--------

           if(testCase.employmentStatus == ''){
            cy.employmentStatusSelect(defaultTestData.employmentStatus)
           }else{
            cy.employmentStatusSelect(testCase.employmentStatus)
           }
            
            cy.livingSituationSelect(testCase.propertyOwner)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- Finance details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
            cy.provideLvr(defaultTestData.lvr)
            cy.provideTerm(defaultTestData.termInYears)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert
           
            cy.checkTheInputFieldInterestValue(testCase.lenderName,testCase.interestRateMin)
            cy.checkTheInterestValue(testCase.lenderName, testCase.interestRateMin, testCase.interestRateMax)

        })

        it(testCase.lenderName + ",LoanTerm = 6years, Asset Year - " + testCase.manufactureYear + ", " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect(defaultTestData.financeType)
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(testCase.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(testCase.manufactureYear)

            // --------Customer--------

           if(testCase.employmentStatus == ''){
            cy.employmentStatusSelect(defaultTestData.employmentStatus)
           }else{
            cy.employmentStatusSelect(testCase.employmentStatus)
           }
            
            cy.livingSituationSelect(testCase.propertyOwner)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- Finance details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
            cy.provideLvr(defaultTestData.lvr)
            cy.provideTerm(defaultTestData.termInYears+1)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert
            
            cy.checkTheInputFieldInterestValue(testCase.lenderName,parseFloat(testCase.interestRateMin)+0.5)
            cy.checkTheInterestValue(testCase.lenderName, (parseFloat(testCase.interestRateMin)+0.5).toString(), (parseFloat(testCase.interestRateMax)+0.5).toString())

        })
    })
})