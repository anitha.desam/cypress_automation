///<reference types = "Cypress"/>
let testData = require('../../../../../fixtures/json_files/Consumer/pepperKnockoutRules.json')
let defaultTestData = require('../../../../../fixtures/defaultTestData.json')

describe('Pepper Consumer Tier Validation', () => {

    const pepperConsumerKnockoutRulesTests = testData.testScenarios;
    beforeEach(() => {
        cy.visit("/")
    })
    pepperConsumerKnockoutRulesTests.forEach((testCase) => {

        it(testCase.lenderName + " -> " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect(defaultTestData.financeType)
            cy.applicationTypeSelect(defaultTestData.applicationType)
            if (testCase.purchaseSource == "") {
                cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            } else {
                cy.purchaseSourceSelect(testCase.purchaseSource)
            }

            if (testCase.assetAge == '') {
                cy.vehicleConditionSelect("New")
            }
            else {
                cy.vehicleConditionSelect("Used")
            }

            if (testCase.category == 'AssetAge' || testCase.category == 'Residual') {
                cy.manufactureYearClick().manufactureYearSelect(new Date().getFullYear() - testCase.assetAge)
            }
            else {
                cy.manufactureYearClick().manufactureYearSelect(new Date().getFullYear() - 1)
            }


            // --------Customer--------

            if (testCase.employmentStatus == '') {
                cy.employmentStatusSelect(defaultTestData.employmentStatus)
            } else {
                cy.employmentStatusSelect(testCase.employmentStatus)
            }

            if (testCase.category == "LengthOfEmployment") {
                cy.currentEmploymentDurationFrequency(testCase.currentEmploymentDurationFrequency)
                cy.currentEmploymentDuration(testCase.lengthOfCurrentEmployment)

            }

            if (testCase.livingSituation == "") {
                cy.livingSituationSelect(defaultTestData.livingSituation)
            } else {
                cy.livingSituationSelect(testCase.livingSituation)
            }

           
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)

            if (testCase.category == 'CustomerAge') {
                cy.dateOfBirth(testCase.customerAge);
            }

            if (testCase.category == 'ResidencyStatus') {
                cy.citizenship(testCase.residencyStatus)
            }

            if (testCase.savings == "") {

            } else {
                cy.addSavings(testCase.savings)
            }


            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)


            if (testCase.category == 'CreditScore') {
                cy.creditScore(testCase.creditScore)
            }

            //(testCase.hadPreviousFinanceSelect == "") ? cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance) : cy.hadPreviousFinanceSelect(testCase.hadPreviousFinance)

            if (testCase.category == 'CreditHistory') {
                cy.creditHistorySelect(testCase.creditHistory)
            } else {
                cy.creditHistorySelect(defaultTestData.creditHistory)
            }

           

            //----- Finance details -----

            if (testCase.purchasePrice == "") {
                cy.provideAssetPrice(defaultTestData.assetPrice)
            } else {
                cy.provideAssetPrice(testCase.purchasePrice)
            }

            //cy.provideLvr(defaultTestData.lvr)
            if (testCase.category == 'AssetAge' || testCase.category == 'Residual') {
                cy.termFrequency("Months")
                cy.provideTerm(testCase.term)

            }
            else {
                cy.provideTerm(defaultTestData.termInYears)
            }

            if (testCase.category == 'Residual') {
                cy.provideResidualPercentage(testCase.residualPercent)
            }

            if (testCase.lengthOfPreviousEmployment != '') {
                // cy.get("div[role=button]").eq(17).click();
                // cy.contains("Months").click();
                //cy.previousEmploymentDurationFrequency("Months")
                cy.previousEmploymentDuration(testCase.lengthOfPreviousEmployment)

            }
            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert

            cy.knockoutRuleValidation(testCase.lenderName, testCase.resultMessage, testCase.failureMessage)

        })

    })
})