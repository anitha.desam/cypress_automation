///<reference types = "Cypress"/>
let testData = require('../../../../../fixtures/json_files/Consumer/macleaseRateCard.json')
let defaultTestData = require('../../../../../fixtures/defaultTestData.json')

describe('Maclease Consumer Rate-Card', () => {

    const macleaseRateCardTests = testData.testScenarios;
    beforeEach(() => {
        cy.visit("/")
    })
    macleaseRateCardTests.forEach((testCase) => {

        it("Asset Year - " + testCase.manufactureYear + ", " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect(defaultTestData.financeType)
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(testCase.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(testCase.manufactureYear)

            // --------Customer--------

            cy.employmentStatusSelect(defaultTestData.employmentStatus)
            cy.livingSituationSelect(testCase.propertyOwner)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- Finance details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
            cy.provideLvr(testCase.lvr)
            cy.provideTerm(defaultTestData.termInYears)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert
            cy.checkTheInputFieldInterestValue(testCase.lenderName, parseFloat(testCase.interestRateMin)+0.5)
            cy.checkTheInterestValue(testCase.lenderName, testCase.interestRateMin, testCase.interestRateMax)
        })

        it("Asset Year - " + (testCase.manufactureYear - 1) + ", " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect(defaultTestData.financeType)
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(testCase.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(testCase.manufactureYear - 1)

            // --------Employment--------

            cy.employmentStatusSelect(defaultTestData.employmentStatus)
            cy.livingSituationSelect(testCase.propertyOwner)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- asset details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
            
            cy.provideLvr(testCase.lvr)
            cy.provideTerm(defaultTestData.termInYears)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert
            
            cy.checkTheInputFieldInterestValue(testCase.lenderName, parseFloat(testCase.interestRateMin)+0.5)
            cy.checkTheInterestValue(testCase.lenderName, testCase.interestRateMin, testCase.interestRateMax)


        })
    })
})