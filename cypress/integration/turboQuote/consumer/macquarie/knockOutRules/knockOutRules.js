///<reference types = "Cypress"/>
let testData = require('../../../../../fixtures/json_files/Consumer/macleaseKnockOutRules.json')
let defaultTestData = require('../../../../../fixtures/defaultTestData.json')

describe('Maclease Consumer Knockout rules', () => {

    const macleaseConsumerKnockoutRulesTests = testData.testScenarios;

    macleaseConsumerKnockoutRulesTests.forEach((testCase) => {
        if (testCase.status == "") {
            it(testCase.testCaseName, () => {
                cy.visit("/")
                cy.assetTypeSelect(defaultTestData.assetType)
                cy.financeTypeSelect(defaultTestData.financeType)
                cy.applicationTypeSelect(defaultTestData.applicationType)
                cy.purchaseSourceSelect(defaultTestData.purchaseSource)



                if (testCase.category == 'AssetAge') {
                    cy.vehicleConditionSelect("Used")
                }
                else {
                    cy.vehicleConditionSelect("New")
                }

                if (testCase.category == 'AssetAge') {
                    cy.manufactureYearClick().manufactureYearSelect(new Date().getFullYear() - testCase.assetAge)
                }
                else {
                    cy.manufactureYearClick().manufactureYearSelect(new Date().getFullYear() - 1)
                }

                // --------Customer--------

                if (testCase.category == 'EmploymentStatus') {

                    cy.employmentStatusSelect(testCase.currentEmploymentType)

                    if (testCase.currentEmploymentDuration == '') {

                    } else {
                        cy.currentEmploymentDurationFrequency(testCase.currentEmploymentDurationFrequency)
                        cy.currentEmploymentDuration(testCase.currentEmploymentDuration)
                    }
                } else {
                    cy.employmentStatusSelect("Part-Time")
                }

                cy.livingSituationSelect(defaultTestData.livingSituation)
                cy.provideTimeAtResidence(defaultTestData.timeAtAddress)

                if (testCase.category == 'PreviousFinance') {

                    cy.get("div[role=button]").eq(22).click();
                    cy.contains("Yes").next().click();
                    if (testCase.incomeAfterTax == '') {

                    } else {
                        cy.incomeAfterTax(testCase.incomeAfterTax)
                    }

                } else {
                    cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
                }


                if (testCase.category == 'CreditHistory') {
                    cy.creditHistorySelect(testCase.creditHistory)
                } else {
                    cy.creditHistorySelect(defaultTestData.creditHistory)
                }



                if (testCase.category == 'LicenceType') {
                    cy.driverLicenceType(testCase.driversLisenseType)
                }

                if (testCase.category == 'Residual' || testCase.category == 'NAF' || testCase.category == 'PreviousFinance') {
                    cy.provideAssetPrice(testCase.purchasePrice)
                }
                else if (testCase.category == 'CustomerAge') {
                    if (testCase.purchasePrice == "") {
                        cy.provideAssetPrice(defaultTestData.assetPrice)
                    } else {
                        cy.provideAssetPrice(testCase.purchasePrice)
                    }
                }
                else {
                    cy.provideAssetPrice(defaultTestData.assetPrice)
                }
                if (testCase.category == 'CustomerAge') {
                    cy.dateOfBirth(testCase.customerAge);
                }
                if (testCase.category == 'ResidencyStatus') {
                    cy.citizenship(testCase.residencyStatus)
                }
                if (testCase.category == 'CreditScore') {
                    cy.creditScore(testCase.creditScore)
                }

                // --------Finance--------

                if (testCase.category == 'Residual') {
                    cy.provideResidualPercentage(testCase.residual)
                }

                if (testCase.category == 'LoanTerm' || testCase.category == 'Residual' || testCase.category == 'AssetAge') {
                    cy.termFrequency("Months")
                    cy.provideTerm(testCase.term)

                }
                else {
                    cy.provideTerm(defaultTestData.termInYears)
                }

                if (testCase.category == 'LVR') {
                    cy.provideLvr(testCase.lvr)
                }
                else if (testCase.category == 'CustomerAge') {
                    if (testCase.lvr == "") {
                        cy.provideLvr(defaultTestData.lvr)
                    } else {
                        cy.provideLvr(testCase.lvr)
                    }
                }
                else {
                    cy.provideLvr(defaultTestData.lvr)

                }

                //------ Insurance and Warrenty-----  

                if (testCase.category == 'InsuranceAndWarranty') {
                    if (testCase.testCaseName == 'GAP Capitalisation = True , should be invalid') {
                        cy.APICapitalise()
                    }
                    else if (testCase.testCaseName == 'LEX Capitalisation = True, should be invalid') {
                        cy.LEXCapitalise()
                    }
                    else {
                        cy.LPICapitalise()
                    }
                }

                //------ calculate quote-----

                cy.contains("Calculate Quote").click();

                cy.knockoutRuleValidation(testCase.lenderName, testCase.resultMessage, testCase.failureMessage)

                // cy.get("[name = MacleaseConsumer]").find("span").then(($lender) => {
                //     if ($lender.text().includes("Cannot Quote !")) {

                //         expect($lender.text()).to.not.include("Rate chart used")
                //         expect($lender.text()).to.have.string(testCase.resultMessage)
                //         cy.get("[name = MacleaseConsumer]").find("li").then(($lenderres) => {
                //             expect($lenderres.text()).to.have.string(testCase.failureMessage)
                //             assert.equal($lenderres.text(), testCase.failureMessage, 'quote failed')

                //         })

                //     }
                //     else {

                //         expect($lender.text()).to.not.include("Cannot Quote !")
                //         expect($lender.text()).include("Rate chart used")
                //         expect($lender.text()).to.have.string(testCase.resultMessage)



                //     }
                // })

            })
        }

    })
})