///<reference types = "Cypress"/>
let testData = require('../../../../fixtures/json_files/Consumer/commercialMacleaseKnockOutRules.json')
let defaultTestData = require('../../../../fixtures/defaultTestData.json')
let apiConfigData = require('../../../../fixtures/turboQuoteapi.json')
let hook = require('../../../../support/hook.js')

describe('MacCommercial KnockoutRules Test', () => {
    const macleaseRateCardTests = testData.testScenarios;

    macleaseRateCardTests.forEach((testCase) => {
        
        let amountRequired = 50000

        it(testCase.testCaseName, () => {
            let requestBody = {
                "requestedLoanAmount":(testCase.purchasePrice=="")?defaultTestData.assetPrice:testCase.purchasePrice,
                "purchasePrice": (testCase.purchasePrice=="")?defaultTestData.assetPrice:testCase.purchasePrice,
                "condition": (testCase.assetAge=="")?"new":"used",
                "term": (testCase.term=="")?defaultTestData.termInMonths:testCase.term,
                "year": (testCase.assetAge=="")?defaultTestData.manufactureYear:(new Date().getFullYear() - testCase.assetAge),
                "loanToValueRatio":(testCase.lvr=="")?defaultTestData.lvr:testCase.lvr,
                "assetType": "Car",
                "financeType": "ChattelMortgage",
                "propertyOwner": defaultTestData.livingSituation,
                "residualPercent":testCase.residual,
                "birthdate":(testCase.customerAge == "")?"1990-06-17":(new Date().getFullYear() - testCase.customerAge + "-" + (new Date().getMonth()+1) + "-" + new Date().getDate()),
                "creditImpairments":(testCase.creditHistory=="")?"NoImpairments":testCase.creditHistory,
                "isAPIInsuranceCapitalised":(testCase.isAPIInsuranceCapitalised=="")?false:true,
                "isLEXInsuranceCapitalised":(testCase.isLEXInsuranceCapitalised=="")?false:true,
                "isLPIInsuranceCapitalised":(testCase.isLPIInsuranceCapitalised=="")?false:true


            };
            cy.request({
                url: apiConfigData.turboQuoteApi,
                method: apiConfigData.method,
                headers: {
                    'x-api-key': apiConfigData.xApiKey,
                    'content-type': apiConfigData.contentType
                },
                body: requestBody

            }).then((response) => {
                var quoteSize = response.body.quotes
               
                for (let x = 0; x < quoteSize.length; x++) {
                    if(quoteSize[x].lenderCode == "macCommercial"){
                        if(testCase.resultMessage=="Rate chart used"){
                            expect(quoteSize[x].canQuote).to.be.true;
                        }else{
                            if(testCase.category=="NAF"){
                                console.log(quoteSize[x].naf)
                            }
                            expect(quoteSize[x].canQuote).to.be.false;
                            //for(let m = 0; m < quoteSize[m].message.length;m++){
                                expect(quoteSize[x].message[0]).to.eq(testCase.failureMessage)
                            //}
                           
                        }
                    }

                }

            })
 
        })

    })

})