///<reference types = "Cypress"/>
let testData = require('../../../../fixtures/json_files/Consumer/commercialMacleaseRateCard.json')
let apiConfigData = require('../../../../fixtures/turboQuoteapi.json')
let hook = require('../../../../support/hook.js')

describe('Mac-Commercial ratecard testCases', () => {
    const macleaseRateCardTests = testData.testScenarios;

    macleaseRateCardTests.forEach((testCase) => {
        
        let amountRequired = 150000

        it("Asset Year - " + testCase.manufactureYear + ", " + testCase.testCaseName, () => {
            cy.request({
                url: apiConfigData.turboQuoteApi,
                method: apiConfigData.method,
                headers: {
                    'x-api-key': apiConfigData.xApiKey,
                    'content-type': apiConfigData.contentType
                },
                body:
                {
                    "requestedLoanAmount": amountRequired,
                    "purchasePrice": amountRequired,
                    "condition": testCase.vehicleCondition,
                    "year": testCase.manufactureYear,
                    "term": 60,
                    "loanToValueRatio": testCase.lvr,
                    "assetType": "Car",
                    "financeType": "ChattelMortgage",
                    "propertyOwner": testCase.propertyOwner

                }

            }).then((response) => {
                if (response.body.quotes[1].lenderCode == "macCommercial") {
                    let responseData = response.body.quotes[1]
                    console.log(responseData)
                    expect(responseData.floorRate).to.eq(parseFloat(testCase.interestRateMin))
                    expect(responseData.lenderProductCode).to.eq(testCase.lenderProductCode)
                    //expect(responseData.brokerageIncGst).to.be.closeTo(hook.macCommercialMaxBrokerage(testCase.maxBrokerage,amountRequired+660+350),0.4)
                    expect(responseData.dealOriginationFeeIncGst).to.eq(660)
                    expect(responseData.fixedFees[0].feeValue).to.eq(350)
                    expect(responseData.lenderAccountKeepingFee).to.eq(8.25)
                    
                }

            })
 
        })

    })

})