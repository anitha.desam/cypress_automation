///<reference types = "Cypress"/>
let testData = require('../../../../fixtures/json_files/Consumer/commercialMacleaseLowDocRules.json')
let apiConfigData = require('../../../../fixtures/turboQuoteapi.json')
let defaultTestData = require('../../../../fixtures/defaultTestData.json')


describe('MacleaseCommercial LowDoc Eligibility', () => {
    const macleaseRateCardTests = testData.testScenarios;

    macleaseRateCardTests.forEach((testCase) => {

        let amountRequired = 50000

        it(testCase.testCaseName + " , ABN Status " + testCase.abnStatus + " , GST Status " + testCase.gstStatus, () => {
            cy.request({
                url: apiConfigData.turboQuoteApi,
                method: apiConfigData.method,
                headers: {
                    'x-api-key': apiConfigData.xApiKey,
                    'content-type': apiConfigData.contentType
                },
                body:
                {
                    "purchasePrice": (testCase.assetPrice == "") ? amountRequired : testCase.assetPrice,
                    "condition": (testCase.assetAge == "") ? "new" : "used",
                    "term": (testCase.term == "") ? defaultTestData.termInMonths : testCase.term,
                    "year": (testCase.assetAge == "") ? defaultTestData.manufactureYear : (new Date().getFullYear() - testCase.assetAge),
                    "assetType": "Car",
                    "financeType": "ChattelMortgage",
                    "propertyOwner": (testCase.propertyOwnership == "") ? "Homeowner" : testCase.propertyOwnership,
                    "abnStatus": (testCase.abnStatus == "") ? "" : testCase.abnStatus,
                    "abnRegisteredDuration": (testCase.abnRegisteredDuration == "") ? 0 : parseInt(testCase.abnRegisteredDuration),
                    "gstStatus": (testCase.gstStatus == "") ? "" : testCase.gstStatus,
                    "gstRegisteredDuration": (testCase.gstRegisteredDuration == "") ? 0 : parseInt(testCase.gstRegisteredDuration),
                    "depositAmount": (testCase.deposit == "") ? 0 : testCase.deposit,
                    "purchaseSource": (testCase.purchaseSource == "") ? "" : testCase.purchaseSource

                }

            }).then((response) => {
                var quoteSize = response.body.quotes
                for (let x = 0; x < quoteSize.length; x++) {
                    if (quoteSize[x].lenderCode == "macCommercial") {
                        expect(quoteSize[x].lowDocEligibility).to.eq(testCase.eligibilityStatus)
                        if (quoteSize[x].lowDocEligibility == "InEligible") {
                            expect(quoteSize[x].lowDocFailureReason).to.eq(testCase.failureMessage)
                        }
                    }

                }

            })

        })

    })

})