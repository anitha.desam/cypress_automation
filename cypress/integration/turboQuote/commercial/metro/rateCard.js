///<reference types = "Cypress"/>
let testData = require('../../../../fixtures/json_files/Consumer/metroRateCard.json')
let defaultTestData = require('../../../../fixtures/defaultTestData.json')
let utils = require('../../../../support/hook')


describe('Maclease Consumer Rate-Card', () => {

    const macleaseRateCardTests = testData.testScenarios;
    beforeEach(() => {
        cy.visit("/")
    })
    macleaseRateCardTests.forEach((testCase) => {

        it("Asset Year - " + testCase.manufactureYear + ", " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect("Chattel Mortgage")
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(defaultTestData.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(defaultTestData.manufactureYear)

            // --------Customer--------

            cy.employmentStatusSelect(defaultTestData.employmentStatus)
            cy.livingSituationSelect(defaultTestData.livingSituation)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- Finance details -----

            cy.provideAssetPrice(testCase.assetPrice)
            //cy.provideLvr(testCase.lvr)
            cy.termFrequency("Months")
            cy.provideTerm(testCase.termInMonths)
            

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert
           // cy.checkTheInputFieldInterestValue(testCase.lenderName,testCase.interestRateMin)
            //cy.checkTheInterestValue(testCase.lenderName, testCase.interestRateMin, testCase.interestRateMax)
            //console.log(utils.repaymentMonthly(testCase.naf,11.52,testCase.termInMonths))
            console.log(utils.repaymentMonthly(45990,6.74,24))
            cy.get("[name = "+ testCase.lenderName +"]").should(($div) => {
               // console.log($div.get().innerText)
                let lenderText = $div.get(0).innerText
                console.log(lenderText)
                let lenderNameText = lenderText.slice(0, lenderText.indexOf("\n"))
                expect(lenderNameText).to.eq(LenderAndTier)
            })  

        })

        xit("Asset Year - " + (testCase.manufactureYear - 1) + ", " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect(defaultTestData.financeType)
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(testCase.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(testCase.manufactureYear - 1)

            // --------Employment--------

            cy.employmentStatusSelect(defaultTestData.employmentStatus)
            cy.livingSituationSelect(testCase.propertyOwner)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- asset details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
            
            cy.provideLvr(testCase.lvr)
            cy.provideTerm(defaultTestData.termInYears)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert
            
            cy.checkTheInputFieldInterestValue(testCase.lenderName,testCase.interestRateMin)
            cy.checkTheInterestValue(testCase.lenderName, testCase.interestRateMin, testCase.interestRateMax)


        })
    })
})