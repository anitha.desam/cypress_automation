
Cypress.Commands.add("checkTheInterestValue",(lenderName,floorRate,ceilingRate) =>{
    
    cy.get("[name = "+ lenderName +"]").find(".rc-slider-handle").invoke('attr', 'aria-valuemin').should('equal', floorRate)
    cy.get("[name = "+ lenderName +"]").find(".rc-slider-handle").invoke('attr', 'aria-valuemax').should('equal', (parseFloat(ceilingRate)).toString())
}) 

Cypress.Commands.add("checkTheInputFieldInterestValue",(lenderName , inputFieldInterestRate) =>{

    cy.get("[name = "+ lenderName +"]").find("#customer-rate-textfield").should('have.value',inputFieldInterestRate.toString())
    
}) 

Cypress.Commands.add("validateLenderAndLenderTier",(lenderName, LenderAndTier) =>{

    cy.get("[name = "+ lenderName +"]").should(($div) => {
        let lenderText = $div.get(0).innerText
        let lenderNameText = lenderText.slice(0, lenderText.indexOf("\n"))
        expect(lenderNameText).to.eq(LenderAndTier)
    })   
    
})

Cypress.Commands.add("knockoutRuleValidation",(lenderName, resultMessage, failureMessage) =>{

    cy.get("[name = "+ lenderName +"]").find("span").then(($lender) => {
        if ($lender.text().includes("Cannot Quote !")) {

            expect($lender.text()).to.not.include("Rate chart used")
            expect($lender.text()).to.have.string(resultMessage)
            cy.get("[name = "+ lenderName +"]").find("li").then(($lenderres) => {
                expect($lenderres.text()).to.have.string(failureMessage)
                assert.equal($lenderres.text(), failureMessage, 'quote failed')

            })

        }
        else {

            expect($lender.text()).to.not.include("Cannot Quote !")
            expect($lender.text()).include("Rate chart used")
            expect($lender.text()).to.have.string(resultMessage)



        }
    })  
    
})

Cypress.Commands.add("getNafFieldValue",() =>{
    cy.get("[name = "+ testCase.lenderName +"]").within(($lender) =>{
            
        $lender.find("td").eq(3).text().replace(/[^\d.-]/g,'')

})

})

Cypress.Commands.add("getCommissionFieldValue",() =>{
    cy.get("[name = "+ testCase.lenderName +"]").within(($lender) =>{
            
        let naf = $lender.find("td").eq(3).text()
        let nafValue = naf.replace(/[^\d.-]/g,'')
        console.log(nafValue)
        let brokerage = Math.round((nafValue*(4/100))/1.1)
        console.log(Math.round(brokerage))

        //let brokerageValue = brokerage.replace(/[^\d.-]/g,'')

        let brokerageVal= $lender.find("td").eq(5).text()
        let brokerageValue= Math.round(brokerageVal.replace(/[^\d.-]/g,''))
        console.log("Brok="+ brokerageValue)
        expect(brokerageValue).to.eql(brokerage);
    })

})
