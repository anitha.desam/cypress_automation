
Cypress.Commands.add("employmentStatusSelect",(employmentStatus) =>{
    cy.get("div[role=button]").eq(15).click();
    cy.contains(employmentStatus).click();
})

Cypress.Commands.add("currentEmploymentDuration",(currentEmpDuration) =>{
    cy.get("#lengthOfEmployment").type(currentEmpDuration);
})

Cypress.Commands.add("currentEmploymentDurationFrequency",(currentEmpDurationFreq) =>{
    cy.get("div[role=button]").eq(16).click();
    cy.contains(currentEmpDurationFreq).click();
})

Cypress.Commands.add("livingSituationSelect",(livingSituation) =>{
    cy.get("div[role=button]").eq(18).click();
    cy.contains(livingSituation).click();
})

Cypress.Commands.add("provideTimeAtResidence",(TimeAtResidence) =>{
    cy.get('input[id=lengthOfResidence]').type(TimeAtResidence)
})

Cypress.Commands.add("dateOfBirth",(customerAge,) =>{

    let getCurrentMon = new Date().getMonth();
    if(getCurrentMon < 9) {
        getCurrentMon = '0'+(getCurrentMon+1);
    }else{
        getCurrentMon = getCurrentMon+1;
    }
    let getCurrentDate = new Date().getDate();
    if(getCurrentDate < 10) {
        getCurrentDate= '0'+getCurrentDate;
    }
    cy.get("#dateOfBirth").clear().type(new Date().getFullYear() - customerAge + "-" + getCurrentMon + "-" + getCurrentDate) 
    
})

Cypress.Commands.add("citizenship",(residencyStatus)=>{
    cy.get("div[role=button]").eq(20).click();
    cy.contains(residencyStatus).click();
})

Cypress.Commands.add("driverLicenceType",(licenceType)=>{
    cy.get("div[role=button]").eq(21).click();
    cy.contains(licenceType).click();
})

Cypress.Commands.add("hadPreviousFinanceSelect",(hadPreviousFinance) =>{
    cy.get("div[role=button]").eq(22).click();
    cy.contains(hadPreviousFinance).click();
})

Cypress.Commands.add("addSavings",(savings) =>{
    cy.get("#savings").type(savings);
})

Cypress.Commands.add("incomeAfterTax",(income) =>{
    cy.get("#incomeAfterTax").type(income);
})

Cypress.Commands.add("creditHistorySelect",(creditHistory) =>{
    cy.get("div[role=button]").eq(24).click();
    cy.contains(creditHistory).click();
})
Cypress.Commands.add("creditScore",(creditScore) =>{
    cy.get("#credit-score").type(creditScore)
})


    
    



