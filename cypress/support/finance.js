Cypress.Commands.add("provideAssetPrice",(assetPrice) =>{
    cy.get('input[id=asset-price]').type(assetPrice)
})

Cypress.Commands.add("provideResidualAmount",(residual) =>{
    cy.get('input[id=residual]').type(residual)
})

Cypress.Commands.add("provideResidualPercentage",(residualPercent) =>{
    cy.get('input[id=residual-percent]').type(residualPercent)
})

Cypress.Commands.add("provideDepositAmount",(depositAmount) =>{
    cy.get('input[id=deposit-required]').type(depositAmount)
})

Cypress.Commands.add("provideLvr",(lvr) =>{
    cy.get('input[id=loan-value-ratio]').clear().type(lvr)
})

Cypress.Commands.add("provideTerm",(term) =>{
    cy.get('input[id=term]').clear().type(term)
})

Cypress.Commands.add("termFrequency",(freq) =>{
    cy.contains("Years").click()
    cy.contains(freq).click()
   // cy.get("div[role=button]").eq(29).click().contains(freq).click();
})