
    Cypress.Commands.add("assetTypeSelect",(assetType) =>{
        cy.get("div[role=button]").eq(2).click();
        cy.contains(assetType).click();
    })

    Cypress.Commands.add("financeTypeSelect",(financeType) =>{
        cy.get("div[role=button]").eq(3).click();
    cy.contains(financeType).click();
    })

    Cypress.Commands.add("applicationTypeSelect",(applicationType) =>{
        cy.get("div[role=button]").eq(4).click();
        cy.contains(applicationType).click();
    })

    Cypress.Commands.add("vehicleConditionSelect",(vehicleCondition) =>{
        cy.get("div[role=button]").eq(5).click();
        cy.contains(vehicleCondition).click();
    })

    Cypress.Commands.add("purchaseSourceSelect",(purchaseSource) =>{
        cy.get("div[role=button]").eq(6).click();
        cy.contains(purchaseSource).click();
    })

    Cypress.Commands.add("manufactureYearClick",() =>{
        cy.get("div[role=button]").eq(7).click();
    })

    Cypress.Commands.add("manufactureYearSelect",(manufactureYear) =>{
        cy.contains(manufactureYear).click();
    })

    



   


//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
