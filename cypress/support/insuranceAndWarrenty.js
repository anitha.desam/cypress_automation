Cypress.Commands.add("APIPremium",(apiPremium) =>{
    cy.get('#api-insurance-premium').clear().type(apiPremium);
})
Cypress.Commands.add("APICapitalise",() =>{
    cy.get('#api-insurance-capitalise').click();
})

Cypress.Commands.add("LEXPremium",(lexPremium) =>{
    cy.get('#lex-insurance-premium').clear().type(lexPremium);
})
Cypress.Commands.add("LEXCapitalise",() =>{
    cy.get('#lex-insurance-capitalise').click();
})

Cypress.Commands.add("LPIPremium",(lpiPremium) =>{
    cy.get('#lpi-insurance-premium').clear().type(lpiPremium);
})
Cypress.Commands.add("LPICapitalise",() =>{
    cy.get('#lpi-insurance-capitalise').click();
})