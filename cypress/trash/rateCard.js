///<reference types = "Cypress"/>
let testData = require('../fixtures/json_files/Consumer/commercialMacleaseRateCard.json')
let defaultTestData = require('../fixtures/defaultTestData.json')
let hoo = require('../support/hook.js')

describe('Maclease Consumer Rate-Card', () => {

    const macleaseRateCardTests = testData.testScenarios;
    beforeEach(() => {
        cy.visit("/")
    })
    macleaseRateCardTests.forEach((testCase) => {

        it("Asset Year - " + testCase.manufactureYear + ", " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect("Chattel Mortgage")
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(testCase.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(testCase.manufactureYear)

            // --------Customer--------

            cy.employmentStatusSelect(defaultTestData.employmentStatus)
            cy.livingSituationSelect(testCase.propertyOwner)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- Finance details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
            cy.provideLvr(testCase.lvr)
            cy.provideTerm(defaultTestData.termInYears)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert

            //cy.checkTheInputFieldInterestValue(testCase.lenderName,testCase.interestRateMax)
            //cy.checkTheInterestValue(testCase.lenderName, testCase.interestRateMin, testCase.interestRateMax)
            //expect(cy.getNafFieldValue)
           console.log( hoo.repaymentMonthlyWithBalloon(40556, 5.25, 60,15, 40000))
           //console.log( hoo.repaymentMonthly(41447, 4.95, 60))
            //  cy.get("[name = "+ testCase.lenderName +"]").within(($lender) =>{
            
            //     let naf = $lender.find("td").eq(3).text()
            //     let nafValue = naf.replace(/[^\d.-]/g,'')
            //     console.log(nafValue)
            //     let brokerage = Math.round((nafValue*(4/100))/1.1)
            //     console.log(Math.round(brokerage))

            //     //let brokerageValue = brokerage.replace(/[^\d.-]/g,'')

            //     let brokerageVal= $lender.find("td").eq(5).text()
            //     let brokerageValue= Math.round(brokerageVal.replace(/[^\d.-]/g,''))
            //     console.log("Brok="+ brokerageValue)
            //     expect(brokerageValue).to.eql(brokerage);

            //  })

        })

        xit("Asset Year - " + (testCase.manufactureYear - 1) + ", " + testCase.testCaseName, () => {

            cy.assetTypeSelect(defaultTestData.assetType)
            cy.financeTypeSelect("Chattel Mortgage")
            cy.applicationTypeSelect(defaultTestData.applicationType)
            cy.vehicleConditionSelect(testCase.vehicleCondition)
            cy.purchaseSourceSelect(defaultTestData.purchaseSource)
            cy.manufactureYearClick()
            cy.manufactureYearSelect(testCase.manufactureYear - 1)

            // --------Employment--------

            cy.employmentStatusSelect(defaultTestData.employmentStatus)
            cy.livingSituationSelect(testCase.propertyOwner)
            cy.provideTimeAtResidence(defaultTestData.timeAtAddress)
            cy.hadPreviousFinanceSelect(defaultTestData.hadPreviousFinance)
            cy.creditHistorySelect(defaultTestData.creditHistory)

            //----- asset details -----

            cy.provideAssetPrice(defaultTestData.assetPrice)
            
            cy.provideLvr(testCase.lvr)
            cy.provideTerm(defaultTestData.termInYears)

            //------ calculate quote-----

            cy.contains("Calculate Quote").click();

            //------ lender result assert
            
            cy.checkTheInputFieldInterestValue(testCase.lenderName,testCase.interestRateMax)
            cy.checkTheInterestValue(testCase.lenderName, testCase.interestRateMin, testCase.interestRateMax)


        })
    })
})